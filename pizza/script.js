
$(document).ready(function() {

  var vSizeS = [];
  var vSizeM = [];
  var vSizeL = [];

    $.ajax({
      url: "http://localhost:8080/campaigns/devcamp-date",
      type: "GET",
      data: "json",
      async: false,
      success: function(res) {
        $("#h1-km").html(res);
      },
      error: function(error){
        console.log(error.responseText);
      }
    });

    $.ajax({
      url: "http://localhost:8080/combomenu/combomenu",
      type: "GET",
      data: "json",
      async: false,
      success: function(res) {
        loadDataToPricing(res);
      },
      error: function(error) {
        console.log(error.responseText);
      }
    });

    function loadDataToPricing(paramRes) {
      for (var i = 0; i< paramRes.length; i++) {
        if (paramRes[i].size == "S") {
          $('#h3-s').html(paramRes[i].size + " (Small)");
          $('#dk-s').html("Đường kính: <b>" + paramRes[i].duongKinh + " cm</b>");
          $('#sn-s').html("Sườn nướng: <b>" + paramRes[i].suon + " cây</b>");
          $('#sl-s').html("Salad: <b>" + paramRes[i].salad + " gr</b>");
          $('#nn-s').html("Nước ngọt: <b>" + paramRes[i].soLuongNuocNgot + " chai</b>");
          $('#pr-s').html(paramRes[i].donGia);
        }
        if (paramRes[i].size == "M") {
          $('#h3-m').html(paramRes[i].size + " (Medium)");
          $('#dk-m').html("Đường kính: <b>" + paramRes[i].duongKinh + " cm</b>");
          $('#sn-m').html("Sườn nướng: <b>" + paramRes[i].suon + " cây</b>");
          $('#sl-m').html("Salad: <b>" + paramRes[i].salad + " gr</b>");
          $('#nn-m').html("Nước ngọt: <b>" + paramRes[i].soLuongNuocNgot + " chai</b>");
          $('#pr-m').html(paramRes[i].donGia);
        }
        else {
          $('#h3-l').html(paramRes[i].size + " (Large)");
          $('#dk-l').html("Đường kính: <b>" + paramRes[i].duongKinh + " cm</b>");
          $('#sn-l').html("Sườn nướng: <b>" + paramRes[i].suon + " cây</b>");
          $('#sl-l').html("Salad: <b>" + paramRes[i].salad + " gr</b>");
          $('#nn-l').html("Nước ngọt: <b>" + paramRes[i].soLuongNuocNgot + " chai</b>");
          $('#pr-l').html(paramRes[i].donGia);
        }
      }
    }
});